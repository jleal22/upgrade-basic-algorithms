// Iteración #4: Arrays

// 1.1 Consigue el valor "HULK" del array de cars y muestralo por consola.
let avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];
console.log("1.1 ->", avengers[0]);

// 1.2 Cambia el primer elemento de avengers a "IRONMAN"
avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];
avengers[0] = "IRONMAN";
console.log("1.2 ->", avengers);

// 1.3 Console numero de elementos en el array usando la propiedad correcta de Array.
avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];
console.log("1.3 Numero de elementos ->", avengers.length);

// 1.4 Añade 2 elementos al array: "Morty" y "Summer". 
// Muestra en consola el último personaje del array
let rickAndMortyCharacters = ["Rick", "Beth", "Jerry"];
rickAndMortyCharacters.push("Morty", "Summer");
console.log("1.4 Último personaje ->", rickAndMortyCharacters[rickAndMortyCharacters.length - 1]);


// 1.5 Elimina el último elemento del array y muestra el primero y el último por consola.
rickAndMortyCharacters = ["Rick", "Beth", "Jerry", "Morty", "Summer", "Lapiz Lopez"];
rickAndMortyCharacters.pop();
console.log("1.5 Primer elemento ->", rickAndMortyCharacters[0]);
console.log("1.5 Ultimo elemento ->", rickAndMortyCharacters[rickAndMortyCharacters.length - 1]);

// 1.6 Elimina el segundo elemento del array y muestra el array por consola.
rickAndMortyCharacters = ["Rick", "Beth", "Jerry", "Morty", "Summer", "Lapiz Lopez"];
rickAndMortyCharacters.splice(1, 1);
console.log("1.6 Segundo elemento eliminado -> ", rickAndMortyCharacters);